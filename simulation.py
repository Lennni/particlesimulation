import electron
import GUI
import random
import SETINGS
import threading

electrons = []
from electron import Object
def code(code, addition_x, addition_y, start_id):
    code = code.split("\n")
    print(code)
    particles = []
    for command in code:
        if command[0]!="#":
            command=command.split("!")

            if command[0] == "neutron":
                Object_type = "N"
                particle = Object(start_id, True)
            elif command[0] == "electron":
                Object_type = "E"
                particle = Object(start_id)
            elif command[0] == "proton":
                Object_type = "P"
                particle = Object(start_id)
            elif command[0] == "object":
                Object_type = "O"
                particle = Object(start_id)
            elif command[0] == "sandbox":
                Object_type = "S"
            if Object_type != "S":
                arguments = command[1].split("|")
                print(command)
                print(arguments)
                i = 0
                while True:
                    try:
                        arguments[i] = float(arguments[i])
                    except ValueError:
                        arguments[i] = None
                    except IndexError:
                        break
                    i += 1
                print(particles)
                particle.init(arguments[0] + addition_x, arguments[1] + addition_y,
                              arguments[2], arguments[3], arguments[4], arguments[5])
                start_id += 1
                particles = particles + [particle]
    return particles


def give_electronsBy_elc(name, x=0, y=0, id=0):  # return electrons and takes file
    file = open("saves/" + name + ".elc", mode="r")
    return code(file.read(), x, y, id)


def rand(multyblication=1, neg=False):
    if neg:
        return 2 * (multyblication / 2 - random.random() * multyblication)
    else:
        return random.random() * multyblication


"""for i in range(0, 10):
    if random.randint(0, 4) == 0:
        new_electron = electron.Object(i, True)
        new_electron.init(velocety_x=rand(neg=True), size=5, velocety_y=rand(neg=True), x=rand(45), y=rand(45),
                          Mass=rand(10))
        electrons = electrons + [new_electron]
    else:
        new_electron = electron.Object(i)
        new_electron.init(velocety_x=rand(neg=True), size=5, velocety_y=rand(neg=True), x=rand(45), y=rand(45),
                          Mass=rand(20, True))
        electrons = electrons + [new_electron]
    SETINGS.ID = i"""

# electrons = give_electronsBy_elc("simple.elc")

while True:
    for elec in electrons:
        elec.velocety_by_Objects(electrons)
        for elec2 in electrons:
            if elec.kollision(elec2):
                pass
        elec.prall()
        elec.move()
    GUI_Output = GUI.update(electrons)
    if GUI_Output["pause"]:
        if GUI_Output["worlddestroy"]:
            electrons = []
        electrons = electrons + GUI_Output["newParticles"]
        pass
    else:
        while True:
            GUI_Output = GUI.update(electrons)
            if GUI_Output["worlddestroy"]:
                electrons = []
            electrons = electrons + GUI_Output["newParticles"]
            if GUI_Output["pause"]:
                break

GUI.end()
