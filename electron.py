import math
sqrt = lambda n: n ** 0.5
sin = math.sin
cos = math.cos
arcustangens = math.atan
squared = lambda n: n ** 2
e = math.e
abs = lambda n: -n if n < 0 else n


def gravitation(gravitation_object, energized_object):
    x_differenze = gravitation_object.X - energized_object.X
    y_differenze = gravitation_object.Y - energized_object.Y
    return (-2 * (x_differenze + y_differenze) * e ** (
            -squared(x_differenze + y_differenze) / 10) / gravitation_object.mass) * energized_object.mass


class seen_Object():
    def __init__(self, object, you):
        self.X = object.X - you.X
        self.Y = object.Y - you.Y
        self.size = object.Size
        self.mass = object.mass
        self.distance = sqrt(squared(self.X) + squared(self.Y))

    def gravitation(self):
        try:
            gravitation_factor = self.mass / (self.distance)
        except ZeroDivisionError:
            print("whaaaat")
            print(self.mass, self.distance)
            return "haha jetzt kriegt ihr ne exception"
        return ((self.X) * gravitation_factor, (self.Y) * gravitation_factor)


class Object():
    X = 0
    Y = 0
    x_velocety = 0
    y_velocety = 0
    Size = 1
    mass = 5
    neutron = False

    def __init__(self, ID, neutron=False):
        self.id = ID
        self.neutron = neutron

    def init(self, x=None, y=None, size=None, velocety_x=None, velocety_y=None, Mass=None):
        if x == None:
            x = self.X
        self.X = x

        if y == None:
            y = self.Y
        self.Y = y

        if size == None:
            size = self.Size
        self.Size = size

        if velocety_x == None:
            velocety_x = self.x_velocety
        self.x_velocety = velocety_x

        if velocety_y == None:
            velocety_y = self.y_velocety
        self.y_velocety = velocety_y

        if Mass == None:
            Mass = self.mass
        self.mass = Mass

    def kollision(self, other_Object):
        if other_Object.id != self.id:
            diff_x = self.X - other_Object.X
            diff_y = self.Y - other_Object.Y
            if self.Size + other_Object.Size > ((diff_x ** 2) + (diff_y ** 2)) ** 0.5:
                return True
            else:
                return False
        else:
            return False

    def in_Zone(self, X_line, Y_line):

        if X_line < self.X:
            self.X = X_line
        elif self.X < 0:
            self.X = 0
        if Y_line < self.Y:
            self.Y = Y_line
        elif self.Y < 0:
            self.Y = 0

    def move(self):

        self.X = self.X + self.x_velocety
        self.Y = self.Y + self.y_velocety
        #self.x_velocety = 0  # self.x_velocety * 0.9
        #self.y_velocety = 0  # self.y_velocety * 0.9

    def angle(self) -> float:
        try:
            return math.atan(self.x_velocety / self.y_velocety)
        except ZeroDivisionError:
            return math.pi / 2

    def power(self):
        return self.mass * self.speed()

    def speed(self):
        return sqrt(squared(self.x_velocety) + squared(self.y_velocety))

    def prall(self):
        speed = self.speed()
        angle = self.angle()
        #print(self.id, angle, self.x_velocety, self.y_velocety)
        if angle > (math.pi/2 - 0.1):
            angle = 0
        angle = angle + 0.1
        self.x_velocety = sin(angle) * speed
        self.y_velocety = cos(angle) * speed

    def velocety_by_Objects(self, all_Objects):
        if self.neutron:
            for object in all_Objects:
                if object.id != self.id:
                    other_object = seen_Object(object, self)
                    vector = other_object.gravitation()
                    if object.neutron: self.mass *=-1
                    if other_object.mass > 0:
                        self.x_velocety += vector[0] / (100 * self.mass)
                        self.y_velocety += vector[1] / (100 * self.mass)
                    else:
                        self.x_velocety -= vector[0] / (100 * self.mass)
                        self.y_velocety -= vector[1] / (100 * self.mass)
                    if object.neutron: self.mass *= -1
        else:
            for object in all_Objects:
                if object.id != self.id:
                    if object.neutron:
                        other_object = seen_Object(object, self)
                        vector = other_object.gravitation()
                        self.x_velocety += vector[0] / abs(100 * self.mass)
                        self.y_velocety += vector[1] / abs(100 * self.mass)
                    else:
                        other_object = seen_Object(object, self)
                        vector = other_object.gravitation()
                        self.x_velocety += vector[0] / (-100 * self.mass)
                        self.y_velocety += vector[1] / (-100 * self.mass)