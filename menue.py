from tkinter import *
import SETINGS as SE


def archivate(electrons):
    code = "#object!x,y,size,vx,vy,mass"
    for electron in electrons:
        code = code + "\n"
        if electron.neutron:
            code = code + "neutron!"
        else:
            code = code + "object!"
        code = code + str(electron.X) + "|" + str(electron.Y) + "|" + str(electron.Size) + "|" + str(
            electron.x_velocety) + "|" + str(electron.y_velocety) + "|" + str(electron.mass)
    return code


def Post_configuration():
    def configure_settings():
        if BuildEntry1.get() == "":
            pass
        else:
            SE.Build_file_name1 = BuildEntry1.get()
        if BuildEntry2.get() == "":
            pass
        else:
            SE.Build_file_name2 = BuildEntry2.get()
        if BuildEntry3.get() == "":
            pass
        else:
            SE.Build_file_name3 = BuildEntry3.get()

    tk = Tk()
    tk.configure(bg='#1E1E23')
    canvas = Canvas(tk, width=150, height=10)
    canvas.pack()
    Discription_Label = Label(tk, text="name of the save")
    BuildEntry3 = Entry(tk, bd=5, width=20)
    BuildEntry2 = Entry(tk, bd=5, width=20)
    BuildEntry1 = Entry(tk, bd=5, width=20)
    Submit_Button = Button(tk, text="Submit", command=configure_settings)
    Discription_Label.pack()
    BuildEntry1.pack()
    BuildEntry2.pack()
    BuildEntry3.pack()
    Submit_Button.pack()
    tk.mainloop()


def save(particles):
    def save_particles():
        try:
            file = open("Saves/" + filename_entry.get() + ".elc", mode="x")
        except FileExistsError:
            file = open("Saves/" + filename_entry.get() + ".elc", mode="w")
        file.write(archivate(particles))

    tk = Tk()
    filename_entry = Entry(tk, bd=5, width=20)
    save_button = Button(tk, bd=5, width=10, text="Save", command=save_particles)
    filename_entry.pack()
    save_button.pack()
    tk.mainloop()


def menue():
    tk = Tk()
    setting_button = Button(tk, text="go to settings", command=settings)

    setting_button.pack()
    tk.mainloop()


def settings():
    tk = Tk()
    setting_button = Button(tk, text="go to view settings", command=settings_view)

    setting_button.pack()
    tk.mainloop()


def settings_view():
    def view_setting_change():
        import SETINGS

    tk = Tk()
    zoom_label = Label(tk, text="Zoom:")
    zoom_entry = Entry(tk, bd=5, width=40)
    shift_Label = Label(tk, text="Shift:")
    shift_right = Entry(tk, bd=5, width=20)
    shift_up = Entry(tk, bd=5, width=20)
    submit_button = Button(tk, bd=5, width=20, command=view_setting_change, text="Submit")
    zoom_label.pack()
    zoom_entry.pack()
    shift_Label.pack()
    shift_right.pack()
    shift_up.pack()
    submit_button.pack()
    tk.mainloop()


if __name__ == "__main__":
    Post_configuration()
