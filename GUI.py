import pygame
import SETINGS
from menue import save, menue, Post_configuration

from electron import Object


def code(code, addition_x, addition_y, start_id):
    code = code.split("\n")
    print(code)
    particles = []
    for command in code:
        if command[0]!="#":
            command=command.split("!")

            if command[0] == "neutron":
                Object_type = "N"
                particle = Object(start_id, True)
            elif command[0] == "electron":
                Object_type = "E"
                particle = Object(start_id)
            elif command[0] == "proton":
                Object_type = "P"
                particle = Object(start_id)
            elif command[0] == "object":
                Object_type = "O"
                particle = Object(start_id)
            elif command[0] == "sandbox":
                Object_type = "S"
            if Object_type != "S":
                arguments = command[1].split("|")
                print(command)
                print(arguments)
                i = 0
                while True:
                    try:
                        arguments[i] = float(arguments[i])
                    except ValueError:
                        arguments[i] = None
                    except IndexError:
                        break
                    i += 1
                print(particles)
                particle.init(arguments[0] + addition_x, arguments[1] + addition_y,
                              arguments[2], arguments[3], arguments[4], arguments[5])
                start_id += 1
                particles = particles + [particle]
    return particles
def give_electronsBy_elc(name, x=0, y=0, id=0):  # return electrons and takes file
    file = open("saves/" + name + ".elc", mode="r")
    return code(file.read(), x, y, id)


pygame.init()
win = pygame.display.set_mode((500, 500))
pygame.display.set_caption("electron simulation")
zoom = 5
shift = [0, 0]
sqrt = lambda n: n ** 0.5
squared = lambda n: n ** 2

Pause_Button_Image = pygame.image.load("Imiges/Button_Pause.png")
Run_Button_Image = pygame.image.load("Imiges/Button_Run.png")
Speed_Button = pygame.image.load("Imiges/Speed_Button.png")
BackToAverage = pygame.image.load("Imiges/Button_BackToMiddle.png")
Save_Button = pygame.image.load("Imiges/Save_Button.png")
Post_Button = pygame.image.load("Imiges/Post_Button.png")
Load_Button = pygame.image.load("Imiges/load_Button.png")
Destroy_Button = pygame.image.load("Imiges/Destroy_Button.png")
Configure_Post = pygame.image.load("Imiges/Configure_Post.png")

pause = True
post_electrons = []
post_2_electrons = []
post_3_electrons = []


def averageFromElectron(electrons):
    average_x, average_y = 0, 0
    lenth = len(electrons)
    for el in electrons:
        average_x += el.X
        average_y += el.Y
    return [(average_x / lenth) / zoom, (average_y / lenth) / zoom]


grafic_mode = 0
if grafic_mode == 5:
    Space_Backround = pygame.image.load("Imiges/Space_Backround.png")

destroy_world = False
def update(electrons):
    global zoom, shift, pause, post_electrons, post_2_electrons, post_3_electrons, result_post_electrons, result_post_2_electrons, result_post_3_electrons , destroy_world
    # print(zoom, shift)
    pygame.time.delay(int(SETINGS.speed))
    if grafic_mode == 0:
        pass
    elif grafic_mode == 1:
        pygame.draw.rect(win, (30, 30, 35), (0, 0, 500, 500))
    elif grafic_mode == 2:
        pygame.draw.rect(win, (0, 0, 0), (0, 0, 500, 500))
    elif grafic_mode == 3:
        pygame.draw.rect(win, (0, 0, 30), (0, 0, 500, 500))
    elif grafic_mode == 4:
        pygame.draw.rect(win, (0, 0, 15), (0, 0, 500, 500))
    elif grafic_mode == 5:
        win.blit(Space_Backround, shift)
    elif grafic_mode == 6:
        pygame.draw.rect(win, (255, 255, 255), (0, 0, 500, 500))
    elif grafic_mode == 7:
        pygame.draw.rect(win, (35, 15, 10), (0, 0, 500, 500))
    for electron in electrons:
        if electron.neutron:
            pygame.draw.circle(win, (232, 232, 20),
                               (int(zoom * (shift[0] + electron.X)), int(zoom * (shift[1] + electron.Y))),
                               int(electron.Size * zoom))
        else:
            if electron.mass < 0:
                pygame.draw.circle(win, (0, 50, 200),
                                   (int(zoom * (shift[0] + electron.X)), int(zoom * (shift[1] + electron.Y))),
                                   int(electron.Size * zoom))
            else:
                pygame.draw.circle(win, (196, 36, 0),
                                   (int(zoom * (shift[0] + electron.X)), int(zoom * (shift[1] + electron.Y))),
                                   int(electron.Size * zoom))
        myfont = pygame.font.SysFont("monospace", 10)
        label = myfont.render(str(electron.angle())[0:4], 1, (100, 100, 0))
        win.blit(label, (int(zoom * (shift[0] + electron.X)), int(zoom * (shift[1] + electron.Y))))

    mouse_is_down = pygame.mouse.get_pressed()[0] == 1
    mouse_x, mouse_y = pygame.mouse.get_pos()
    # events buttons and zoom and other stuff:
    for event in pygame.event.get():
        # zoooom
        if str(event)[7] == "5":
            if str(event)[-20] == "4":
                zoom = zoom * 1.1
                shift[0] = shift[0] * 1
                shift[1] = shift[1] * 1
            elif str(event)[-20] == "5":
                zoom = zoom / 1.1
                shift[0] = shift[0] / 1
                shift[1] = shift[1] / 1
    # change shift:
    change = pygame.mouse.get_rel()
    if mouse_is_down:
        shift[0] = shift[0] + (change[0] / zoom)
        shift[1] = shift[1] + (change[1] / zoom)
    # buttons:
    # draw buttons
    if pause:
        win.blit(Pause_Button_Image, (0, 0))
    else:
        win.blit(Run_Button_Image, (0, 0))
    win.blit(Speed_Button, (50, 0))
    win.blit(BackToAverage, (100, 0))
    win.blit(Save_Button, (150, 0))
    win.blit(Load_Button, (200, 0))
    win.blit(Post_Button, (250, 0))
    win.blit(Destroy_Button, (300, 0))
    win.blit(Configure_Post, (350, 0))
    pygame.draw.line(win, (50, 100, 200), (0, 50), (500, 50), 4)
    for i in range(10):
        pygame.draw.line(win, (50, 100, 200), (i * 50, 50), (i * 50, 0), 4)
    # request buttans and make actions
    if 0 < mouse_y < 50 and mouse_is_down:
        # pause button:
        if 0 < mouse_x < 50:
            if pause:
                pause = False
            else:
                pause = True
        # button 2
        if 50 < mouse_x < 100:
            if 0 < mouse_y < 25:
                SETINGS.slower()
            else:
                SETINGS.faster()
        if 100 < mouse_x < 150:
            shift = averageFromElectron(electrons)
        elif 150 < mouse_x < 200:
            save(electrons)
        if 200 < mouse_x < 250:
            destroy_world = True
            print("du solltest ein lade ding programmieren und nichts das die welt zerstört")
        if 250 < mouse_x < 300:
            post_electrons = True
            result_post_electrons = []
        if 300 < mouse_x < 350:
            destroy_world = True
        if 350 < mouse_x < 400:
            Post_configuration()
        if 400 < mouse_x < 450:
            post_2_electrons = True
            result_post_2_electrons = []
        if 450 < mouse_x < 500:
            post_3_electrons = True
            result_post_3_electrons = []
    else:
        destroy_world = False
    # for post new particles
    if not (post_electrons):
        result_post_electrons = []
    if not (post_2_electrons):
        result_post_2_electrons = []
    if not (post_3_electrons):
        result_post_3_electrons = []
    if not (0 < mouse_y < 50) and mouse_is_down and post_electrons:
        result_post_electrons = give_electronsBy_elc(
            SETINGS.Build_file_name1, (mouse_x / zoom) - shift[0], (mouse_y / zoom) - shift[1], SETINGS.ID + 5
        )
        SETINGS.ID = SETINGS.ID + 5 + len(result_post_electrons)
        post_electrons = False
    if not (0 < mouse_y < 50) and mouse_is_down and post_2_electrons:
        result_post_2_electrons = give_electronsBy_elc(
            SETINGS.Build_file_name2, (mouse_x / zoom) - shift[0], (mouse_y / zoom) - shift[1], SETINGS.ID + 5
        )
        SETINGS.ID = SETINGS.ID + 5 + len(result_post_2_electrons)
        post_2_electrons = False
    if not (0 < mouse_y < 50) and mouse_is_down and post_3_electrons:
        result_post_3_electrons = give_electronsBy_elc(
            SETINGS.Build_file_name3, (mouse_x / zoom) - shift[0], (mouse_y / zoom) - shift[1], SETINGS.ID + 5
        )
        SETINGS.ID = SETINGS.ID + 5 + len(result_post_3_electrons)
        post_3_electrons = False
    print(result_post_electrons, result_post_2_electrons, result_post_3_electrons)
    new_particles = result_post_electrons + result_post_2_electrons + result_post_3_electrons
    pygame.display.update()
    return {"pause": pause, "newParticles": new_particles, "worlddestroy": destroy_world}


def end():
    pygame.quit()
